from random import randint

name = input("Hi! What is your name? ")

guess_number = 1
month_number = randint(1, 12)
year_number = randint(1924, 2004)

for num in range(5):
    print("Guess", guess_number, ":", name, "were you born in",
            month_number, "/", year_number, "?")

    response = input("yes or no? ")

    if response == "yes":
        print("I knew it!")
        exit()
    elif guess_number < 5:
        print("Drat! Lemme try again!")
        guess_number += 1
    else:
        print("I have other things to do. Good bye.")